--- a/include/autoconf.hin
+++ b/include/autoconf.hin
@@ -1017,6 +1017,10 @@ Added missing headers after gettext upda
  */
 #	undef USE_ZLIB
 
+/* debian-specific */
+#define XFACE_ABLE
+#define NNTP_SERVER_FILE "/etc/news/server"
+
 /*
  * signal-types
  */
