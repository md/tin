#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
msgid ""
msgstr ""
"Project-Id-Version: tin 1:1.7.1+20030828-1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-03-20 19:01+0100\n"
"PO-Revision-Date: 2020-01-03 08:01+0200\n"
"Last-Translator: Marco d'Itri <md@linux.it>\n"
"Language-Team: IT <tp@lists.linux.it>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../tin.templates:3
msgid "Enter the fully qualified domain name of your news server"
msgstr "Inserire il nome a domini qualificato del news server"

#. Type: string
#. Description
#: ../tin.templates:3
msgid ""
"What news server (NNTP server) should be used for reading and posting news?"
msgstr ""
"Quale news server (server NNTP) deve essere usato per leggere e spedire le "
"news?"
